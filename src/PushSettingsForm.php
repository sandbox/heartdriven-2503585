<?php
/**
 * @file
 * Contains \Drupal\push\PushSettingsForm
 */
namespace Drupal\push;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure push settings.
 */
class PushSettingsForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'push_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'push.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('push.settings');

    $form['enable_push'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Enable push'),
      '#default_value' => $config->get('enable_push'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('push.settings')
      ->set('enable_push', $form_state->getValue('enable_push'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
